import React, { Component } from "react";
import { View } from "react-native";
import { Container } from "native-base";
import { StackNavigator } from "react-navigation";
import RespondersList from "./components/RespondersList";
import Rooms from "./components/Rooms";
import RoutesList from "./components/RoutesList";
import SolutionCreation from "./components/SolutionCreation";
import ComsCreation from "./components/ComsCreation";
import PolygonCreation from "./components/PolygonCreation";

import { ApolloClient } from "apollo-client";
import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloProvider } from "react-apollo";

const client = new ApolloClient({
  link: new HttpLink({
    uri: "https://api.graph.cool/simple/v1/cjdxghvu90bjh018506dqzy2a"
  }),
  cache: new InMemoryCache()
});

const RootStack = StackNavigator(
  {
    RoomsList: {
      screen: Rooms
    },
    Voies: {
      screen: RoutesList
    },
    Poly: {
      screen: PolygonCreation
    },
    Responders: {
      screen: RespondersList
    },
    RouteCreation: {
      screen: SolutionCreation
    },
    CreateComments: {
      screen: ComsCreation
    }
  },
  {
    headerMode: "none"
  }
);

export default class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <Container>
          <RootStack />
        </Container>
      </ApolloProvider>
    );
  }
}
