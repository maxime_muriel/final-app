import React, { Component } from "react";
import {
  ScrollView,
  Dimensions,
  ImageBackground,
  TouchableWithoutFeedback
} from "react-native";
import Svg, { Circle, Polygon } from "react-native-svg";

export default class ClimbingRoutePoly extends Component {
  convertValue = Dimensions.get("window").width / 300;
  width = Dimensions.get("window").width;
  height = 1107 * this.convertValue;

  constructor(props) {
    super(props);
    this.state = { clicks: [], polygonZones: [] };
  }

  handlePress(event) {
    if (this.props.isDrawing === true) {
      const newX = event.nativeEvent.locationX;
      const newY = event.nativeEvent.locationY;

      this.setState({
        clicks: [...this.state.clicks, { x: newX, y: newY }]
      });
    }
  }

  pointsToString(points) {
    let result = "";

    for (let point of points) {
      result = result + point.x.toString() + "," + point.y.toString() + " ";
    }

    return result;
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isDrawing === true && nextProps.isDrawing === false) {
      this.setState({
        polygonZones: [
          ...this.state.polygonZones,
          { points: this.state.clicks }
        ],
        clicks: []
      });
    }
  }

  render() {
    const svgCanvas = (
      <Svg height={this.height} width={this.width}>
        {this.state.clicks.map((click, index) => (
          <Circle
            key={index}
            cx={click.x}
            cy={click.y}
            r={6 * this.convertValue}
            stroke="blue"
            strokeWidth="1"
            fill="blue"
          />
        ))}

        {this.state.polygonZones.map((polygon, index) => {
          return (
            <Polygon
              key={index}
              points={this.pointsToString(polygon.points)}
              fill="none"
              stroke="purple"
              strokeWidth="4"
              onPress={() => this.props.navPoly()}
            />
          );
        })}
      </Svg>
    );

    const toDraw = !this.props.isDrawing ? (
      svgCanvas
    ) : (
      <TouchableWithoutFeedback onPress={evt => this.handlePress(evt)}>
        {svgCanvas}
      </TouchableWithoutFeedback>
    );

    return (
      <ScrollView>
        <ImageBackground
          style={styles.imageStyle}
          resizeMode={"stretch"}
          source={{
            uri:
              "https://files.slack.com/files-pri/T68SNHG2G-F7UUMLL75/image_grimpe_300x1107_px_.jpg?pub_secret=3c6a4b6072"
          }}
        >
          {toDraw}
        </ImageBackground>
      </ScrollView>
    );
  }
}

const styles = {
  imageStyle: {
    paddingBottom: -60,
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
  containerStyle: {
    backgroundColor: "red"
  }
};
