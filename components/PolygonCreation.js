import React, { Component } from "react";
import { View } from "react-native";
import { Header, Body, Title, Container, Left, Button } from "native-base";
import ClimbingRoutePoly from "./ClimbingRoutePoly";
import PolygonFab from "./PolygonFab";
import Icon from "react-native-vector-icons/FontAwesome";

export default class PolygonCreation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      draw: false
    };
  }
  handleToggleDraw() {
    this.setState({ draw: !this.state.draw });
  }
  handlePolyNav() {
    this.props.navigation.navigate("Responders");
  }

  render() {
    return (
      <Container>
        <View style={{ flex: 1 }}>
          <Header>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack(null)}
              >
                <Icon
                  name={"chevron-left"}
                  style={{
                    fontSize: 20,
                    color: "white"
                  }}
                />
              </Button>
            </Left>
            <Body>
              <Title>4c orange</Title>
            </Body>
          </Header>

          <ClimbingRoutePoly
            isDrawing={this.state.draw}
            navPoly={this.handlePolyNav.bind(this)}
          />

          <PolygonFab
            finishedDraw={this.handleToggleDraw.bind(this)}
            isDrawing={this.state.draw}
          />
        </View>
      </Container>
    );
  }
}
