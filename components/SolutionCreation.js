import React, { Component } from "react";
import { View } from "react-native";
import { Header, Body, Title, Container, Left, Button } from "native-base";
import ClimbingRoute from "./ClimbingRoute";
import IconMenu from "./IconMenu";
import Icon from "react-native-vector-icons/FontAwesome";

export default class SolutionCreation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      iconPressed: 0
    };
  }

  handleMenuIconPress(iconPressed) {
    this.setState({ iconPressed: iconPressed });
  }
  handleComsNav() {
    this.props.navigation.navigate("CreateComments");
  }

  render() {
    return (
      <Container>
        <View style={{ flex: 1 }}>
          <Header>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack(null)}
              >
                <Icon
                  name={"chevron-left"}
                  style={{
                    fontSize: 20,
                    color: "white"
                  }}
                />
              </Button>
            </Left>
            <Body>
              <Title>4c orange</Title>
            </Body>
          </Header>

          <ClimbingRoute iconPressed={this.state.iconPressed} />
          <IconMenu
            onMenuIconPress={this.handleMenuIconPress.bind(this)}
            navComs={this.handleComsNav.bind(this)}
          />
        </View>
      </Container>
    );
  }
}
