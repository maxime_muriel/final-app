import React, { Component } from "react";
import { Dimensions } from "react-native";
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Body,
  Title,
  Item,
  Input,
  List,
  ListItem,
  Left
} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";

export default class RoutesList extends Component {
  convertValue = Dimensions.get("window").width / 300;
  width = Dimensions.get("window").width;
  height = 1107 * this.convertValue;

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack(null)}
            >
              <Icon
                name={"chevron-left"}
                style={{
                  fontSize: 20,
                  color: "white"
                }}
              />
            </Button>
          </Left>
          <Body>
            <Title>Laniac - Voies</Title>
          </Body>
        </Header>
        <Content
          searchBar
          rounded
          style={{
            marginLeft: 6 * this.convertValue,
            marginRight: 6 * this.convertValue
          }}
        >
          <Item>
            <Icon name="search" />
            <Input placeholder="Rechercher" />
          </Item>
          <List>
            {routes.map((route, index) => {
              return (
                <ListItem key={index}>
                  <Button
                    transparent
                    dark
                    style={{
                      marginTop: -10 * this.convertValue,
                      marginBottom: -10 * this.convertValue
                    }}
                    onPress={() => this.props.navigation.navigate(route.nav)}
                  >
                    <Text
                      uppercase={false}
                      style={{
                        fontSize: 16
                      }}
                    >
                      {route.name}
                    </Text>
                  </Button>
                </ListItem>
              );
            })}
          </List>
        </Content>
      </Container>
    );
  }
}

const routes = [
  {
    name: "4c orange",
    nav: "Poly"
  },
  {
    name: "4c+ violet",
    nav: ""
  },
  {
    name: "5a jaune",
    nav: ""
  },
  {
    name: "5c bleu",
    nav: ""
  },
  {
    name: "5c+ rouge",
    nav: ""
  }
];
