import React, { Component } from "react";
import { Fab } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";

class PolygonFab extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Fab
        active={this.props.isDrawing}
        position="bottomRight"
        style={{ backgroundColor: this.props.isDrawing ? "orange" : "red" }}
        onPress={() => this.props.finishedDraw()}
      >
        <Icon name={this.props.isDrawing ? "check" : "pencil-square-o"} />
      </Fab>
    );
  }
}

export default PolygonFab;
