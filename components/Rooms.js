import React, { Component } from "react";
import { Dimensions } from "react-native";
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Body,
  Icon,
  Title,
  Item,
  Input,
  List,
  ListItem
} from "native-base";
import gql from "graphql-tag";
import { graphql } from "react-apollo";

class Rooms extends Component {
  convertValue = Dimensions.get("window").width / 300;
  width = Dimensions.get("window").width;
  height = 1107 * this.convertValue;

  render() {
    if (this.props.data.loading) {
      return <Text>Loading</Text>;
    }
    if (this.props.data.error) {
      return <Text>Error</Text>;
    }
    return (
      <Container>
        <Header>
          <Body>
            <Title>Salles</Title>
          </Body>
        </Header>
        <Content
          searchBar
          rounded
          style={{
            marginLeft: 6 * this.convertValue,
            marginRight: 6 * this.convertValue
          }}
        >
          <Item>
            <Icon name="search" />
            <Input placeholder="Rechercher" />
          </Item>

          <List>
            {this.props.data.allRooms.map((room, index) => {
              return (
                <ListItem key={index}>
                  <Button
                    style={{
                      marginTop: -10 * this.convertValue,
                      marginBottom: -10 * this.convertValue
                    }}
                    transparent
                    dark
                    onPress={() => this.props.navigation.navigate("Voies")}
                  >
                    <Text
                      uppercase={false}
                      style={{
                        fontSize: 16
                      }}
                    >
                      {room.name}
                    </Text>
                  </Button>
                </ListItem>
              );
            })}
          </List>
        </Content>
      </Container>
    );
  }
}

const dataRooms = gql`
  query dataRooms {
    allRooms {
      name
      id
    }
  }
`;

export default graphql(dataRooms)(Rooms);
