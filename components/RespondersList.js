import React, { Component } from "react";
import { Dimensions } from "react-native";
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Body,
  Title,
  Left
} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";

export default class RespondersList extends Component {
  convertValue = Dimensions.get("window").width / 300;
  width = Dimensions.get("window").width;
  height = 1107 * this.convertValue;

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack(null)}
            >
              <Icon
                name={"chevron-left"}
                style={{
                  fontSize: 20,
                  color: "white"
                }}
              />
            </Button>
          </Left>
          <Body>
            <Title>4c orange</Title>
          </Body>
        </Header>
        <Content
          style={{
            marginLeft: 4 * this.convertValue,
            marginRight: 4 * this.convertValue
          }}
        >
          {responders.map((responder, index) => {
            return (
              <Button
                key={index}
                block
                info
                onPress={() => console.log("La navigation viendra")}
                style={{
                  marginTop: 10 * this.convertValue
                }}
              >
                <Text>{responder.name}</Text>
              </Button>
            );
          })}

          <Button
            block
            primary
            onPress={() => this.props.navigation.navigate("RouteCreation")}
            style={{
              marginTop: 250 * this.convertValue,
              marginLeft: 40 * this.convertValue,
              marginRight: 40 * this.convertValue
            }}
          >
            <Text>Répondre</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const responders = [
  {
    name: "Luc"
  },
  {
    name: "Marc"
  }
];
