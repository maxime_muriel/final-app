import React, { Component } from "react";
import { Dimensions } from "react-native";
import {
  Container,
  Header,
  Content,
  Button,
  Body,
  Title,
  Left,
  Item,
  Input
} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";

export default class ComsCreation extends Component {
  convertValue = Dimensions.get("window").width / 300;
  width = Dimensions.get("window").width;
  height = 1107 * this.convertValue;

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack(null)}
            >
              <Icon
                name={"chevron-left"}
                style={{
                  fontSize: 20,
                  color: "white"
                }}
              />
            </Button>
          </Left>
          <Body>
            <Title>4c orange</Title>
          </Body>
        </Header>
        <Content>
          <Item
            rounded
            style={{
              height: 100 * this.convertValue,
              marginTop: 30 * this.convertValue
            }}
          >
            <Input placeholder="Commentaires..." />
          </Item>
        </Content>
      </Container>
    );
  }
}
