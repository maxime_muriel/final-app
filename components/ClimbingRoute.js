import React, { Component } from "react";
import {
  ScrollView,
  Dimensions,
  ImageBackground,
  TouchableWithoutFeedback
} from "react-native";
import Svg, { Image } from "react-native-svg";

class ClimbingRoute extends Component {
  constructor(props) {
    super(props);

    this.state = {
      climbingPos: [
        {
          zone: -1,
          link: require("../assets/main_droite.png")
        },
        {
          zone: -1,
          link: require("../assets/main_gauche.png")
        },
        { zone: -1, link: require("../assets/pied_dd.png") },
        { zone: -1, link: require("../assets/pied_gg.png") }
      ]
    };
  }

  handlePress(e) {
    for (let touchableZone of touchableZones) {
      if (
        Math.abs(e.nativeEvent.locationX / convertValue - touchableZone.x) <=
          20 &&
        Math.abs(e.nativeEvent.locationY / convertValue - touchableZone.y) <= 20
      ) {
        this.setState((oldState, props) => {
          const i = props.iconPressed;
          return {
            climbingPos: [
              ...oldState.climbingPos.slice(0, i),
              {
                link: oldState.climbingPos[i].link,
                zone: touchableZones.indexOf(touchableZone)
              },
              ...oldState.climbingPos.slice(i + 1, 4)
            ]
          };
        });
      }
    }
  }

  render() {
    return (
      <ScrollView>
        <ImageBackground
          style={styles.imageStyle}
          resizeMode={"stretch"}
          source={{
            uri:
              "https://files.slack.com/files-pri/T68SNHG2G-F7UUMLL75/image_grimpe_300x1107_px_.jpg?pub_secret=3c6a4b6072"
          }}
        >
          <TouchableWithoutFeedback onPress={e => this.handlePress(e)}>
            <Svg width={width} height={height}>
              {this.state.climbingPos
                .filter(item => item.zone > -1)
                .map((item, index) => {
                  return (
                    <Image
                      key={index}
                      height="15%"
                      width="15%"
                      y={
                        touchableZones[item.zone].y * convertValue -
                        Dimensions.get("window").height * 0.15 / 2
                      }
                      x={
                        touchableZones[item.zone].x * convertValue -
                        Dimensions.get("window").width * 0.15 / 2
                      }
                      href={item.link}
                    />
                  );
                })}
            </Svg>
          </TouchableWithoutFeedback>
        </ImageBackground>
      </ScrollView>
    );
  }
}

const width = Dimensions.get("window").width;
const convertValue = Dimensions.get("window").width / 300;
const height = 1107 * convertValue;
const styles = {
  imageStyle: {
    paddingBottom: -60,
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
  containerStyle: {
    backgroundColor: "red"
  }
};
const touchableZones = [
  { x: 150, y: 30 },
  { x: 225, y: 100 },
  { x: 115, y: 207 },
  { x: 85, y: 280 },
  { x: 120, y: 380 },
  { x: 155, y: 410 },
  { x: 285, y: 470 },
  { x: 68, y: 524 },
  { x: 135, y: 485 },
  { x: 10, y: 588 },
  { x: 133, y: 588 },
  { x: 164, y: 685 },
  { x: 42, y: 750 },
  { x: 230, y: 782 },
  { x: 136, y: 845 },
  { x: 200, y: 945 },
  { x: 200, y: 1025 },
  { x: 257, y: 1082 }
];

export default ClimbingRoute;
