import React, { Component } from "react";
import { Fab, Button, Container } from "native-base";
import { Image } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

class IconMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true"
    };
  }
  render() {
    const { backgroundIconColor, iconStyle } = styles;
    return (
      <Container>
        <Fab
          direction="up"
          style={{ backgroundColor: "#ed2323" }}
          position="bottomLeft"
          onPress={() => this.props.navComs()}
        >
          <Icon name="edit" />

          {icons.map((icon, index) => {
            return (
              <Button
                key={index}
                style={backgroundIconColor}
                onPress={() => this.props.onMenuIconPress(icon.number)}
              >
                <Image style={iconStyle} source={icon.image} />
              </Button>
            );
          })}
        </Fab>
        <Fab
          active={this.state.active}
          position="bottomRight"
          direction="up"
          style={{ backgroundColor: "#ed2323" }}
          onPress={() => console.log("hi")}
        >
          <Icon name="save" />
        </Fab>
      </Container>
    );
  }
}

const styles = {
  backgroundIconColor: {
    backgroundColor: "transparent"
  },
  iconStyle: {
    height: 50,
    width: 50
  },
  footIcon: {}
};

const icons = [
  {
    number: 0,
    image: require("./../assets/main_droite.png")
  },
  {
    number: 1,
    image: require("./../assets/main_gauche.png")
  },
  {
    number: 2,
    image: require("./../assets/pied_dd.png")
  },
  {
    number: 3,
    image: require("./../assets/pied_gg.png")
  }
];

export default IconMenu;
